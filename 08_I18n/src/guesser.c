#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <libintl.h>
#include <locale.h>

#define _(STRING) gettext(STRING)

int main() {
    bindtextdomain("guesser", getenv("PWD"));
    setlocale(LC_ALL, "");
    textdomain("guesser");

    int left = 0;
    int right = 100;
    int guess;

    char answer[4];

    printf(_("Pick a number from 1 to 100\n"));

    while(right > left + 1) {
        guess = (left + right) / 2;
        
        printf(_("Is picked number greater than %d? y/n\n"), guess);

        while (1) {
            scanf("%s", answer);

            if (strcmp(answer, _("y")) == 0) {
                left = guess;
                break;
            } else if (strcmp(answer, _("n")) == 0) {
                right = guess;
                break;
            } else {
                printf(_("Didn't understand your answer :(\nTry again\n"));
            }
        }
    }

    printf(_("The guessed number is %d\n"), right);
}