#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc == 1) {
        char* help =
            "Possible options for the program:\n"
            "    1. ./range N - generate sequence 0, 1, ..., N-1\n"
            "    2. ./range M N - generate sequence M, M+1, ..., N-1\n"
            "    3. ./range M N S - generate sequence M, M+S, ..., N-1\n";
        puts(help);
    } else if (argc == 2) {
        int N = atoi(argv[1]);
        for (int i=0; i<N; ++i) {
            printf("%d\n", i);
        }
    } else if (argc == 3) {
        int M = atoi(argv[1]);
        int N = atoi(argv[2]);
        for (int i=M; i<N; ++i) {
            printf("%d\n", i);
        }
    } else if (argc == 4) {
        int M = atoi(argv[1]);
        int N = atoi(argv[2]);
        int S = atoi(argv[3]);
        for (int i=0; (M+S*i)<N; ++i) {
            printf("%d\n", M+S*i);
        }
    }
}