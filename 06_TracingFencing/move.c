#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Error: run program with arguments: ./move infile outfile\n");
        exit(1);
    }

    int fd_infile;
    fd_infile = open(argv[1], O_RDONLY);
    if (fd_infile < 0) {
        printf("Error: can't open infile: %s\n", argv[1]);
        exit(2);
    }

    // Еhis check must come after infile existence check
    if (strcmp(argv[1], argv[2]) == 0) {
        // infile and outfile is the same file
        return 0;
    }

    int fd_outfile;
    fd_outfile = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fd_outfile < 0) {
        printf("Error: can't open outfile: %s\n", argv[2]);
        exit(3);
    }

    char buf[4096];
    ssize_t nread;
    int saved_errno;
    while (nread = read(fd_infile, buf, sizeof buf), nread > 0) {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_outfile, out_ptr, nread);

            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            } else if (errno != EINTR) {
                printf("Error: unknown error while write: %d\n", errno);

                if (remove(argv[2]) != 0) {
                    printf("Error: can't remove outfile: %s\n", argv[2]);
                }

                exit(4);
            }
        } while (nread > 0);
    }
    if (nread != 0) {
        printf("Error: unknown error while read %s: %d\n", argv[1], errno);

        if (remove(argv[2]) != 0) {
            printf("Error: can't remove outfile: %s\n", argv[2]);
        }

        exit(5);
    }

    if (close(fd_outfile) != 0) {
        printf("Error: can't close outfile: %s\n", argv[2]);

        if (remove(argv[2]) != 0) {
            printf("Error: can't remove outfile: %s\n", argv[2]);
        }

        exit(6);
    }
        
    if (close(fd_infile) != 0) {
        printf("Error: can't close infile: %s\n", argv[1]);

        if (remove(argv[1]) != 0) {
            printf("Error: can't remove infile: %s\n", argv[1]);
        }

        exit(7);
    }

    if (remove(argv[1]) != 0) {
        printf("Error: can't remove infile: %s\n", argv[1]);
        exit(8);
    }

    return 0;
}