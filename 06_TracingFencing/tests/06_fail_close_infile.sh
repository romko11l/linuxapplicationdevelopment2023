#!/bin/bash

echo test_text > infile.txt

strace -e trace=close -e fault=close:error=EIO:when=4 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 7 && test -f outfile.txt || echo "Test 06_fail_close_infile failed."

rm -f ./infile.txt ./outfile.txt
