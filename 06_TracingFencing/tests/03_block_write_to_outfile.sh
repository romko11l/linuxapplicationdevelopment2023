#!/bin/bash

echo test_text > infile.txt

strace -e trace=write -e fault=write:error=EIO:when=1 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 4 && test -f infile.txt || echo "Test 03_block_write_to_outfile failed."

rm -f ./infile.txt ./outfile.txt
