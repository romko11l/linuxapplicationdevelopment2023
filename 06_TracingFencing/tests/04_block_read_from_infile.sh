#!/bin/bash

echo test_text > infile.txt

strace -e trace=read -e fault=read:error=EIO:when=2 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 5 && test -f infile.txt || echo "Test 04_block_read_from_infile failed."

rm -f ./infile.txt ./outfile.txt./
