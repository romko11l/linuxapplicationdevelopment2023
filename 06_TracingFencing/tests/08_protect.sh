#!/bin/bash

echo test_text > infile_PROTECT.txt

LD_PRELOAD=`pwd`/protect.so ./move infile_PROTECT.txt outfile.txt

test $? -eq 0 && test -f infile_PROTECT.txt && test -f outfile.txt || echo "Test 08_protect failed."

rm -f ./infile_PROTECT.txt ./outfile.txt
