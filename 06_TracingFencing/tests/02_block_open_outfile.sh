#!/bin/bash

echo test_text > infile.txt

strace -e trace=openat -e fault=openat:error=EPERM:when=4 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 3 && test -f infile.txt || echo "Test 02_block_open_outfile failed."

rm -f ./infile.txt ./outfile.txt
