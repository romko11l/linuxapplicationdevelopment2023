#!/bin/bash

echo test_text > infile.txt

strace -e trace=unlink -e fault=unlink:error=EIO:when=1 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 8 && test -f infile.txt && test -f outfile.txt || echo "Test 07_block_remove_infile failed."

rm -f ./infile.txt ./outfile.txt
