#!/bin/bash

echo test_text > infile.txt

strace -e trace=close -e fault=close:error=EIO:when=3 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 6 && test -f infile.txt || echo "Test 05_fail_close_outfle failed."

rm -f ./infile.txt ./outfile.txt
