#!/bin/bash

echo test_text > infile.txt

strace -e trace=openat -e fault=openat:error=EPERM:when=3 ./move infile.txt outfile.txt 2> /dev/null

test $? -eq 2 && test -f infile.txt || echo "Test 01_block_open_infile failed."

rm -f ./infile.txt ./outfile.txt
