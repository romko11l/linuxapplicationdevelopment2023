#! /usr/bin/bash

if [ -z $1 ]
then
    DELAY="0.1"
else
    DELAY="$1"
fi

TEXT=""

while IFS= read -r line; do
  LINELEN=${#line}
  TEXT=$TEXT$line
done

LEN=${#TEXT}

declare -a ARRAY

for (( i=1; i <= LEN; i++ )) do
  ARRAY+=($i)
done

SHUFFLEARRAY=( $(shuf -e "${ARRAY[@]}") )

LINELEN=$LINELEN+1

tput clear

for i in ${SHUFFLEARRAY[@]}; do
  x=$(($i%$LINELEN))
  y=$(($i/$LINELEN))
  tput cup ${y} ${x}
  echo -n ${TEXT:${i}:1}
  sleep $DELAY
done

HEIGHT=$(tput lines)
WIDTH=$(tput cols)

tput cup WIDTH HEIGHT
