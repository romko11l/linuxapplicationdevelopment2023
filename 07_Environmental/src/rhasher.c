#include <stdlib.h>
#include <string.h>
#include <rhash.h>

#ifdef READLINE_INPUT
#include <readline/readline.h>
#endif

int hash_string(int hash_id, int result_type, const char* hashable) {
    char digest[64];
    char result[2048];

    int code = rhash_msg(hash_id, hashable, strlen(hashable), digest);
    if (code < 0) {
        fprintf(stderr, "error while compute a hash of the given message\n");
        return code;
    }

    rhash_print_bytes(result, digest, rhash_get_digest_size(hash_id), result_type);
    printf("%s\n", result);

    return 0;
}

int hash_file(int hash_id, int result_type, const char* path) {
    char digest[64];
    char result[2048];

    int code = rhash_file(hash_id, path, digest);
    if (code < 0) {
        fprintf(stderr, "error while compute a hash of file: %s\n", path);
        return code;
    }

    rhash_print_bytes(result, digest, rhash_get_digest_size(hash_id), result_type);
    printf("%s\n", result);

    return 0;
}

int hasher(const char* hash, char* hashable) {
    int result;
    if (!strcmp(hash, "md5")) {
        // md5 + base64
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_MD5, RHPR_BASE64, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_MD5, RHPR_BASE64, hashable);
        }
    } else if (!strcmp(hash, "MD5")) {
        // md5 + hex
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_MD5, RHPR_HEX, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_MD5, RHPR_HEX, hashable);
        }
    } else if (!strcmp(hash, "sha1")) {
        // sha1 + base64
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_SHA1, RHPR_BASE64, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_SHA1, RHPR_BASE64, hashable);
        }
    } else if (!strcmp(hash, "SHA1")) {
        // md5 + hex
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_SHA1, RHPR_HEX, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_SHA1, RHPR_HEX, hashable);
        }
    } else if (!strcmp(hash, "tth")) {
        // tth + base64
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_TTH, RHPR_BASE64, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_TTH, RHPR_BASE64, hashable);
        }
    } else if (!strcmp(hash, "TTH")) {
        // tth + hex
        if (hashable[0] == '"' && hashable[strlen(hashable)-2] == '"') {
            hashable[strlen(hashable)-2] = '\0';
            result = hash_file(RHASH_TTH, RHPR_HEX, (char*)(hashable+sizeof(char)));
        } else {
            result = hash_string(RHASH_TTH, RHPR_HEX, hashable);
        }
    } else {
        fprintf(stderr, "unknown hash\n");
        result = 1;
    }
    return result;
}

int main(int argc, char** argv) {
    size_t len;
    char* line;
    int nread;

    char* algorithm;
    char* argument;

    while(1) {
        printf(">>> ");

        #ifdef READLINE_INPUT
        line = readline(NULL);
        nread = (line) ? 0 : -1;
        #else
        nread = getline(&line, &len, stdin);
        #endif

        if (nread == -1) {
            // get EOF
            break;
        }

        algorithm = strtok(line, " ");
        argument = strtok(NULL, " ");
        if (argument == NULL || strlen(argument) == 1) {
            fprintf(stderr, "Bad input, need command in format: algorithm string/file\n");
            continue;
        }
        
        hasher(algorithm, argument);
    }
}