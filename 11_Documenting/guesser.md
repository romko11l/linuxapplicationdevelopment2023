/// @mainpage
# Number predictor

guesser is an oracle that predicts the number you have chosen from 1 to 100

## Usage

Usage: ./guesser [-\-roman|-r] [-\-version|-v] [-\-help|-h]

## Source

See sources in `src/guesser.c`
