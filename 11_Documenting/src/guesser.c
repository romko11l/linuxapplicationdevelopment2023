/**
 * @file guesser.c
 * @author romko11l
 * @brief Number guesser
 * @version 0.1.0
 * @date 06.12.23
 * 
 * Udage: guesser [OPTIONS]
 * - -\-help, -h        show this message
 * - -\-vesrion, -v     print version
 * - -\-roman, -r       use roman numbers
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <libintl.h>
#include <locale.h>
#include <config.h>

#define _(STRING) gettext(STRING)
#define HELP_TEXT _("guesser is an oracle that predicts the number you have chosen from 1 to 100\n\
\n\
Usage: guesser [OPTIONS]\n\
\n\
    --help, -h      show this message\n\
    --vesrion, -v   print version\n\
    --roman, -r     use roman numbers\n\
\n\
")

static char *roman_nums[] = {
    "N", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
    "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX",
    "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI", "XXVII", "XXVIII", "XXIX",
    "XXX", "XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV", "XXXVI", "XXXVII", "XXXVIII", "XXXIX", "XL",
    "XLI", "XLII", "XLIII", "XLIV", "XLV", "XLVI", "XLVII", "XLVIII", "XLIX", "L",
    "LI", "LII", "LIII", "LIV", "LV", "LVI", "LVII", "LVIII", "LIX", "LX",
    "LXI", "LXII", "LXIII", "LXIV", "LXV", "LXVI", "LXVII", "LXVIII", "LXIX", "LXX",
    "LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV", "LXXVI", "LXXVII", "LXXVIII", "LXXIX",
    "LXXX", "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC",
    "XCI", "XCII", "XCIII", "XCIV", "XCV", "XCVI", "XCVII", "XCVIII", "XCIX", "C"
};

/**
 * Converts number from decimal numeric system to roman numeric system
 * @param num Decimal number from 1 to 100
 * 
 * @returns Point to char representation of number in roman numeric system  
*/
char *dec_to_roman(int num) {
	return roman_nums[num];
}

int main(int argc, char **argv) {
    bindtextdomain("guesser", getenv("PWD"));
    setlocale(LC_ALL, "");
    textdomain("guesser");

    int use_roman_num = 0;
    if (argc > 1) {
        if (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h")) {
            printf(HELP_TEXT);
            return 0;
        } else if (!strcmp(argv[1], "--version") || !strcmp(argv[1], "-v")) {
            printf(VERSION);
            return 0;
        } else if (!strcmp(argv[1], "--roman") || !strcmp(argv[1], "-r")) {
            use_roman_num = 1;
        }
    }

    int left = 0;
    int right = 100;
    int guess;

    char answer[4];

    if (use_roman_num) {
        printf(_("Guess a number from %s to %s\n"), dec_to_roman(1), dec_to_roman(100));
    } else {
        printf(_("Guess a number from 0 to 100\n"));
    }

    while(right > left + 1) {
        guess = (left + right) / 2;

        if (use_roman_num) {
            printf(_("Is guessed number greater than %s? y/n\n"), dec_to_roman(guess));
        } else {
            printf(_("Is guessed number greater than %d? y/n\n"), guess);
        }

        while (1) {
            scanf("%s", answer);

            if (strcmp(answer, _("y")) == 0) {
                left = guess;
                break;
            } else if (strcmp(answer, _("n")) == 0) {
                right = guess;
                break;
            } else {
                printf(_("Didn't understand your answer :(\nTry again\n"));
            }
        }
    }

    if (use_roman_num) {
        printf(_("The guessed number is %s\n"), dec_to_roman(right));
    } else {
        printf(_("The guessed number is %d\n"), right);
    }
}
