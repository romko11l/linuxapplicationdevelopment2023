#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>


// See https://www.gnu.org/software/libc/manual/html_node/Matching-POSIX-Regexps.html
int main(int argc, char** argv) {
    if (argc != 4) {
        printf("Error: run program with arguments: ./esub regexp substitution string\n");
        exit(1);
    }

    char *regex_string = argv[1];
    char *substitution_string = argv[2];
    char *string = argv[3];

    regex_t regex;
    int res;
    res = regcomp(&regex, regex_string, REG_EXTENDED);
    if (res != 0) {
        printf("Error: regex compilation failed\n");

        char errbuf[128];
        regerror(res, &regex, errbuf, sizeof(errbuf));
        printf("%s\n", errbuf);

        exit(1);
    }

    regmatch_t match[1];
    res = regexec(&regex, string, 1, match, 0);
    if (res != 0) {
        regfree(&regex);
        if (res == REG_NOMATCH) {
            // No substrig that satisfies regular expression
            printf("%s\n", string);
            exit(0);
        } else {
            printf("Error: regexec return %d\n", res);

            char errbuf[128];
            regerror(res, &regex, errbuf, sizeof(errbuf));
            printf("%s\n", errbuf);

            exit(1);
        }
    }

    int len_before_match = match[0].rm_so;
    int len_after_match = strlen(string) - match[0].rm_eo;
    int len_substitution = strlen(substitution_string);

    // +1 for '\0' symbol
    char *result_string = malloc((len_before_match + len_substitution + len_after_match + 1) * sizeof(char));

    for (int i=0; i < len_before_match; ++i) {
        result_string[i] = string[i];
    }

    for (int i=0; i < len_substitution; ++i) {
        result_string[i+len_before_match] = substitution_string[i];
    }

    for (int i=0; i < len_after_match; ++i) {
        result_string[i+len_before_match+len_substitution] = string[i+match[0].rm_eo];
    }

    result_string[len_before_match + len_substitution + len_after_match] = '\0';

    printf("%s\n", result_string);

    free(result_string);
    regfree(&regex);
}